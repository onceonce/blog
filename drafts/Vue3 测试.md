Vue3 测试

Web应用开发测试一般分为：

- Unit Testing
- Component Testing
- End-To-End (E2E) Testing

## 单元测试 Unit Testing

- vue应用的单元测试与其他Web开发的单元测试没有较大的区别



如何选择测试框架？

通常来说关于测试的建议都是不依赖框架的（framework-agnostic）， 所以这里是一些评估最适合你应用的单元测试工具基本的指引