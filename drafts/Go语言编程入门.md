# Go语言编程入门

## Web应用

### Web应用定义
- 程序必须向发送命令请求的客户端返回HTML，而客户端泽向用户展示渲染后的HTML
- 程序在想客户端传输数据时 必须使用HTTP协议
### HTTP
HTTP是一种无状态的，由文本构成的请求-响应(request-response)协议。这种协议使用的是客户端-服务器(client-server)计算模型
- 客户端-服务器计算模型中，发送请求的客户端负责向返回响应的服务器发起会话。服务器则向客户端提供服务
- 在HTTP协议中，客户端也被称作用户代理
- 无状态：HTTP唯一知道的就是客户端发起请求，服务器返回响应，对后续和之前发生的请求一无所知。
- 性能：HTTP 1.1 可以通过持久化连接提升性能

### 通用网关接口CGI
- 允许服务器与一个独立运行于web服务器进程之外的进程对接。
- CGI程序： 通过CGI与服务器进行对接的程序被称为CGI程序

### 服务端包含SSI

- 允许在HTML文件包含一些指令（directive）. 当客户端请求一个HTML文件时，服务器在返回这个文件之前，会先执行文件中包含的指令，并将文件中出现指令的位置替换成只写指令的执行结果
- 常见：`PHP`、`ASP`、`JSP`、`ColdFusion`

```php
<html>
　<head><title>Example SSI</title></head>
　<body>
　　<!--#include file="navbar.shtml" -->
　</body>
</html>
```

### Web应用组成

- 处理器 handler。
  - 接收和处理客户端发来的请求，然后调用模板引擎生成HTML并将数据填充至要回传给客户端的响应报文中
  - 用MVC模式来说，处理器既是控制器，也是模型
- 模板引擎 template engine。分为动态模板和静态模板

#### MVC模式： 模型-视图-控制器





## Go语言基础

