---
title:  Fluter开发
date:   2021-06-06 23:49:00
categories: 
 - flutter
typora-root-url: ..
---
## 学习目标
- 彻底入门Flutter开发
- 掌握系统化开发技能
- 理解Flutter项目的开发流程
- 规范的代码编写与工程化封装思想
- 上手中大型Flutter项目
### 学习阶段
1. 基础入门
    - 开发工具准备与环境搭建
    - 快速上手
    - 基础入门
2. 入门实战
    - APP导航框架与常用功能实现
3. 进阶提升
    - 网络编程与数据存储技巧
    - 列表组件
    - Flutter与 Native混合开发
4. 进阶实战
    - App首页功能开发
    - 搜索模块开发
    - 只能AI语音搜索模块实现
    - 旅拍模块实现
5. 进阶拓展
    - 开发包和插件开发指南
    - 折叠屏全面屏适配与兼容问题
    - 打包发布
    - 升级与适配指南
### 三方库
- `pub.dev`
- 官方教程`flutter.dev`

## 0 环境搭建

- 下载并安装`Android Studio`。[官方指南](https://developer.android.google.cn/studio/intro)
- 安装`Flutter`和`Dart`插件并重启`Android Studio`。
- 非必须: flutter镜像配置： [Using Flutter in China](https://flutter.dev/community/china)
- Flutter sdk,解压到安装木录，如 `c:\flutter`
- 添加环境变量。将安装路径配置到path中。如`c:\flutter\bin`